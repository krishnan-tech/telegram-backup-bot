# telegram-backup-bot

Telegram Backup Bot - It is used to backup all the messages to MongoDB.

## Getting started

To start this bot, go to `settings.py` file and change this things

- TELEGRAM_API_ID
- TELEGRAM_API_HASH
- sources (list of all source channels)
- (optional) change MongoDB url
