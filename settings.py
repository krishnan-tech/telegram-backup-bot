import pymongo

# your API_ID and API_HASH - you can get it from https://my.telegram.org/apps
TELEGRAM_API_ID = "xxxxx"
TELEGRAM_API_HASH = "xxxxxxxxxxxxxxxxxxxxxx"

# cluster = pymongo.MongoClient("mongodb+srv://krishnan:krishnan@cluster0.if05q.mongodb.net/telegram?retryWrites=true&w=majority")
cluster = pymongo.MongoClient("mongodb://localhost/backup")
db = cluster["backup"]
DB_COLLECTION = db["backup"]


# channels you want to post this message
sources = [
    "https://t.me/username",
]

# Delay time in each channel in (seconds)
DELAY_TIME = 1     # every 1 seconds
