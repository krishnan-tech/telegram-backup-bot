from msilib.schema import Media
from telethon import TelegramClient, events
import asyncio

from termcolor import colored
from colorama import init
from settings import DB_COLLECTION, TELEGRAM_API_ID, TELEGRAM_API_HASH, DELAY_TIME, sources, db


init()

def setup_db():
    print("EXECUTING SETUP DB")
    
    for identifier in sources:
        is_channel_exists = DB_COLLECTION.find_one({"identifier": identifier})
        if not is_channel_exists:
            print(f"{identifier} is new! Adding to DB")
            DB_COLLECTION.insert_one({
                "identifier": identifier,
                "start": 0,
                "end": 0
            })

def generating_source():
    print("EXECUTING GENERATE_SOURCE")
    result = list(DB_COLLECTION.find({}))
    return result


def update_end_DB(identifier, count):
    print("UPDATING END IN DB")
    DB_COLLECTION.update_one({"identifier": identifier}, {"$set": {"end": count}})

def add_message_to_db(identifier, message):
    CHANNEL_COLLECTION = db[identifier]
    CHANNEL_COLLECTION.insert_one({"message": message})


setup_db()
   

async def main():
    async with TelegramClient("session", TELEGRAM_API_ID, TELEGRAM_API_HASH) as client:
        print(colored("bot started", "cyan"))

        data = generating_source()
        
        for each_source in data:
            identifier = each_source['identifier']
            start = each_source['start']
            end = each_source['end']
            print(identifier, start, end)
            count = end
            async for message in client.iter_messages(identifier, reverse=True, offset_id=end):
                # print(message)
                
                count += 1

                try:

                    if message.file:
                        ext = message.file.ext
                        if (message.media):
                            await client.download_media(message.media, f"media/{message.id}{ext}")

                    add_message_to_db(identifier, message.text)
                    print(colored(f"Added message from {identifier} with message id {str(message.id)}", "green"))
                except Exception as e:
                    print(colored(f"Message with message id {str(message.id)} does not exists", "red"))
                    f = open("errors.txt", "a", encoding="utf-8")
                    f.write(f"[ message id {str(message.id)} ] ->" + str(e) + "\n\n")
                    f.close()
                

                if count % 10 == 0: 
                    # update end every 10 messages
                    update_end_DB(identifier, count)

            # update end on completion of channel
            update_end_DB(identifier, count)

            
asyncio.run(main())

